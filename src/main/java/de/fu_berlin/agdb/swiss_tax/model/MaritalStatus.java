package de.fu_berlin.agdb.swiss_tax.model;

/**
 * Marital status for a person: single, married, widowed..
 */
public enum MaritalStatus {
    CIVIL_UNION('C', "person in civil union"),
    DIVORCED('D', "divorced person"),
    SINGLE('S', "single person"),
    MARRIED('M', "married person"),
    WIDOWED('W', "widowed person"),
    NOT_SPECIFIED(null, "unspecified person");

    private Character code;
    private String description;

    MaritalStatus(Character code, String description) {
        this.code = code;
        this.description = description;
    }

    public Character getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    /**
     * get marital status based on code.
     *
     * @param c code of marital status
     * @return one of marital status enums
     */
    public static MaritalStatus fromCode(char c) {
        for (MaritalStatus m : MaritalStatus.values()) {
            if (m.getCode().equals(c)) {
                return m;
            }
        }
        return MaritalStatus.NOT_SPECIFIED;
    }
}
