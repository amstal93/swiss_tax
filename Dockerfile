# base our image on openjdk14
FROM openjdk:14-jdk
# IMPORTANT:
# run the command: "./mvnw clean package" inside your terminal
# this will compile the jar and copy to the generated target folder

# this copies the .jar file to the image
COPY ./target/*.jar app.jar

ENV JAVA_OPTS=""
# set .jar file as entrypoint for container
ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar /app.jar"]
